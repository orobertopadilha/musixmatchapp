import React, { useState } from 'react'
import { ScrollView, Text } from "native-base"
import { useRoute } from '@react-navigation/core'
import { useEffect } from 'react/cjs/react.development'
import tracksService from '../../service/tracks/TracksService'

const Lyrics = () => {
    const { params } = useRoute()
    const [lyrics, setLyrics] = useState('')

    useEffect(() => {
        const loadLyrics = async () => {
            let result = await tracksService.getLyrics(params.trackId)
            setLyrics(result)
        }

        loadLyrics()
    }, [])

    return (
        <ScrollView padding={4}>
            <Text fontSize="xl" bold>
                {params.trackName}
            </Text>
            <Text italic>{params.artist}</Text>
            <Text marginTop={4}>{lyrics}</Text>
        </ScrollView>
    )
}

export default Lyrics