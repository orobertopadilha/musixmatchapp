import { FlatList, Text } from 'native-base'
import React, { useState, useEffect } from 'react'
import { useCallback } from 'react'
import { SafeAreaView } from 'react-native'

import artistsService from '../../service/artists/ArtistsService'
import ArtistItem from './ArtistItem'

const TopArtists = () => {

    const [artists, setArtists] = useState([])

    useEffect(() => {

        const loadArtists = async () => {
            let result = await artistsService.getTopArtists()
            setArtists(result)
        }

        loadArtists()
    }, [])

    const createArtistItem = useCallback(({ item, index }) => <ArtistItem item={item} index={index} />, [])

    const artistKeyExtractor = useCallback(artist => artist.id, [])

    return (
        <SafeAreaView>
            <FlatList 
                data={artists}
                renderItem={createArtistItem}
                keyExtractor={artistKeyExtractor}
                />
        </SafeAreaView>
    )
}

export default TopArtists