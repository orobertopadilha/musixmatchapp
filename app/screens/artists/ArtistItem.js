import { Avatar, HStack, Text } from 'native-base'
import React from 'react'
import CardView from '../../components/CardView'

const ArtistItem = ({ item, index }) => {
    return (
        <CardView>
            <HStack alignItems="center">
                <Avatar bg="green.700" size="sm">
                    {index + 1}
                </Avatar>
                <Text bold fontSize="md" marginLeft={4}>{item.name}</Text>
            </HStack>
        </CardView>
    )
}

export default ArtistItem