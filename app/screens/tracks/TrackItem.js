import { useNavigation } from '@react-navigation/core'
import { Avatar, HStack, Pressable, Text, VStack } from 'native-base'
import React from 'react'
import CardView from '../../components/CardView'
import { ROUTE_LYRICS } from '../../navigation/AppRoutes'

const TrackItem = ({ item, index }) => {

    const navigation = useNavigation()

    const onItemClick = () => {
        navigation.push(ROUTE_LYRICS, { 
            trackId: item.id,
            trackName: item.name,
            artist: item.artist
        })
    }

    return (
        <Pressable onPress={onItemClick}>
            <CardView>
                <HStack alignItems="center">

                    <Avatar bg="amber.500">
                        {index + 1}
                    </Avatar>

                    <VStack marginLeft={4}>
                        <Text bold fontSize="md">{item.name}</Text>
                        <Text fontSize="xs">{item.artist}</Text>
                        <Text fontSize="xs">{item.album}</Text>
                    </VStack>
                </HStack>
            </CardView>
        </Pressable>
    )
}

export default TrackItem