import { FlatList } from 'native-base'
import React, { useCallback, useEffect, useState } from 'react'
import { SafeAreaView } from 'react-native'

import tracksService from '../../service/tracks/TracksService'
import TrackItem from './TrackItem'

const TopTracks = () => {

    const [tracks, setTracks] = useState([])

    useEffect(() => {
        const loadTracks = async () => {
            let result = await tracksService.getTopTracks()
            setTracks(result)
        }

        loadTracks()
    }, [])

    const createTrackItem = useCallback(({ item, index }) => <TrackItem item={item} index={index} />, [])

    const trackKeyExtractor = useCallback(track => track.id, [])

    return (
        <SafeAreaView flex={1}>
            <FlatList
                data={tracks}
                renderItem={createTrackItem}
                keyExtractor={trackKeyExtractor}
            />
        </SafeAreaView>
    )
}

export default TopTracks