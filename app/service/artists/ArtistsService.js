import { serviceApi } from "../config/ServiceApi"

const getTopArtists = async () => {
    let response = await serviceApi.get('chart.artists.get', {
        params: {
            page: 1,
            page_size: 20,
            country: 'us'
        }
    })

    let artistList = response.data.message.body.artist_list
    let result = artistList.map(res => {
        let artist = res.artist
        return {
            id: artist.artist_id,
            name: artist.artist_name
        }
    })

    return result
}

const artistsService = { getTopArtists }

export default artistsService