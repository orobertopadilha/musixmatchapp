import axios from "axios"

const createApi = () => {
    let instance = axios.create({
        baseURL: 'https://api.musixmatch.com/ws/1.1/',
        params: {
            apikey: '7e5ac541d1febded4f15a6df0bbf4450'
        }
    })

    return instance
}

export const serviceApi = createApi()