import { serviceApi } from "../config/ServiceApi"

const getTopTracks = async () => {
    let response = await serviceApi.get('chart.tracks.get', {
        params: {
            chart_name: 'mxmweekly',
            page: 1,
            page_size: 20,
            country: 'us'
        }
    })

    let trackList = response.data.message.body.track_list
    
    let result = trackList.map(res => {
        let track = res.track
        
        return {
            id: track.track_id,
            name: track.track_name,
            album: track.album_name,
            artist: track.artist_name
        }
    })

    return result
}

const getLyrics = async (trackId) => {
    let response = await serviceApi.get('track.lyrics.get', {
        params: {
            track_id: trackId
        }
    })

    return response.data.message.body.lyrics.lyrics_body
}

const tracksService = { getTopTracks, getLyrics }

export default tracksService
