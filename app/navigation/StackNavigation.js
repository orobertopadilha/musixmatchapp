import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { StatusBar, useTheme } from 'native-base'
import React from 'react'
import Lyrics from '../screens/lyrics/Lyrics'
import { ROUTE_HOME, ROUTE_LYRICS } from './AppRoutes'
import TabNavigation from './TabNavigation'

const Stack = createStackNavigator()

const StackNavigation = () => {
    const { colors } = useTheme()

    const stackScreenOptions = {
        headerStyle: {
            backgroundColor: colors.blue['700']
        },

        headerTintColor: colors.white
    }

    return (
        <NavigationContainer>
            <StatusBar backgroundColor={colors.blue['800']} />
            <Stack.Navigator screenOptions={stackScreenOptions}>
                <Stack.Screen name={ROUTE_HOME} component={TabNavigation} 
                    options={{ headerShown: false }} />
                <Stack.Screen name={ROUTE_LYRICS} component={Lyrics} 
                    options={{ title: 'Track Lyrics'}} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default StackNavigation