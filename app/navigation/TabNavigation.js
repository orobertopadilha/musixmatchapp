import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { useTheme } from 'native-base'

import React from 'react'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import TopArtists from '../screens/artists/TopArtists'
import TopTracks from '../screens/tracks/TopTracks'
import { ROUTE_TOP_ARTISTS, ROUTE_TOP_TRACKS } from './AppRoutes'

const Tab = createBottomTabNavigator()

const TabNavigation = () => {

    const { colors } = useTheme()

    const tabScreenOptions = {
        tabBarActiveBackgroundColor: colors.blue['700'],
        tabBarActiveTintColor: colors.white,

        tabBarInactiveBackgroundColor: colors.blue['700'],
        tabBarInactiveTintColor: colors.blue['400'],

        headerStyle: {
            backgroundColor: colors.blue['700']
        },

        headerTintColor: colors.white
    }

    return (
        <Tab.Navigator screenOptions={tabScreenOptions}>
            <Tab.Screen name={ROUTE_TOP_TRACKS} component={TopTracks}
                options={
                    {
                        title: 'Top Tracks',
                        tabBarIcon: ({ color, size }) => <MaterialIcons name='music-note' color={color} size={size} />
                    }
                } />
            <Tab.Screen name={ROUTE_TOP_ARTISTS} component={TopArtists}
                options={
                    {
                        title: 'Top Artists',
                        tabBarIcon: ({ color, size }) => <MaterialIcons name='mic' color={color} size={size} />
                    }
                }
            />
        </Tab.Navigator>
    )

}

export default TabNavigation