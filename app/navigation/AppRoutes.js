export const ROUTE_TOP_ARTISTS = "top-artists"
export const ROUTE_TOP_TRACKS = "top-tracks"

export const ROUTE_HOME = "home"
export const ROUTE_LYRICS = "lyrics"